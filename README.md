Desenvolva uma aplicação para a web (recorrendo às tecnologias que achar adequadas) que permite ao utilizar lançar e monitorizar em real-time um conjunto de indicadores.
Os tipos de indicadores são definidos pela aplicação, e não é permitido ao utilizador alterar.
Estes são:
A) Cadência fabrico
input: nr peças e tempo de operação em segundos
output: cadência (nr peças/hora)

B) Defeitos
input: defeitos (nr peças com fabricadas com defeito)
output: número de defeitos

C) Consumo de água
input: consumo de água (litros), e tempo de operação estação de lavagem (segundos)
output: consumo de água (m3/hora)

A aplicação deve apresentar uma interface para introduzir novos valores (apenas criar, não é necessário edição ou eliminar). Deve também apresentar uma página ("dashboard") com as seguintes medidas para cada indicador:
1) média
2) última amostra

Notas: Os valores de cada indicador devem ser armazenados numa base de dados não volátil. Em alternativa a aplicação para a web (i.e. com visualização/edição em browser), pode implementar em linguagem Java para Desktop. Neste caso, o input do utilizador deve ser feito em linha de comandos. O "Dashboard" deve ser apresentado em linha de comandos, de forma contínua (isto é, sem necessidade de relançar a aplicação ou interação do utilizador).


*----------------------*

Iniciar o servidor:

Ligar à base de dados MongoDB com o URL "mongodb://localhost:27017/fabrica"
Caso o URL da base de dados não for o definido, o mesmo pode ser alterado na linha 1 do ficheiro mongo.js

Ligar o servidor NodeJS com o comando "node app.js"

Correr a aplicação desktop:

Na pasta desktopapp/ correr:
mvn package
java -cp target/desktopapp-1.0-SNAPSHOT-jar-with-dependencies.jar core.Main