const urldb = 'mongodb://localhost:27017/fabrica';
var mongoose = require('mongoose');

mongoose.connect(urldb, {useNewUrlParser: true});

var cadenciaSchema = new mongoose.Schema({
    pecas: Number,
    tempo: Number,
    data: Date
});

var defeitosSchema = new mongoose.Schema({
    defeitos: Number,
    data: Date
});

var consumoSchema = new mongoose.Schema({
    agua: Number,
    tempo: Number,
    data: Date
});

var Cadencia = mongoose.model('Cadencia', cadenciaSchema);
var Defeitos = mongoose.model('Defeitos', defeitosSchema);
var Consumo = mongoose.model('Consumo', consumoSchema);

module.exports = {
    getCadencia : async function () {
        let result = await Cadencia.find();
        var i = 0;
        var calcCadencia = 0;
        result.forEach(function(cad) {
            i++;
            var hora = 3600 / cad.tempo;
            calcCadencia = cad.pecas * hora; // nr pecas por hora
        });
        if (i != 0)
            calcCadencia /= i; // Media de cadencia
        return new Number(calcCadencia);
    },

    getDefeitos : async function () {
        let result = await Defeitos.find();
        var i = 0;
        result.forEach(function(def) {
            i += def.defeitos;
        });
        return new Number(i);
    },

    getConsumo : async function () {
        let result = await Consumo.find();
        var i = 0;
        var calcM3Hora = 0;

        result.forEach(function(con) {
            var m3 = con.agua / 1000; // converter litros para m3
            var horas = 3600 / con.tempo; // converter segundos para horas
            calcM3Hora += m3 * horas;
            i++;
        });
        if (i != 0)
            calcM3Hora /= i;
        return new Number(calcM3Hora);
    },

    saveCadencia : function (pecas, tempo) {
        if (tempo == 0) {
            console.log("Tempo de operação não pode ser 0");
        } else {
            var data = new Date();
            var cadencia = {pecas, tempo, data};
            var newCadencia = new Cadencia(cadencia);
            newCadencia.save();
        }
    },

    saveDefeitos : function (defeitos) {
        if (defeitos == 0) {
            console.log("Nr de peças com defeitos não pode ser 0");
        } else {
            var data = new Date();
            var newDefeitos = new Defeitos({defeitos, data});
            newDefeitos.save();
        }
    },

    saveConsumo : function (agua, tempo) {
        if (tempo == 0) {
            console.log("Tempo de operação não pode ser 0");
        } else {
            var data = new Date();
            var newConsumo = new Consumo({agua, tempo, data});
            newConsumo.save();
        }
    },

    deleteAll : async function () {
        Cadencia.deleteMany({}, function(err) {});
        Defeitos.deleteMany({}, function(err) {});
        Consumo.deleteMany({}, function(err) {});
    },

    seeAll : async function () {
        var all = await Cadencia.find();
        all += await Defeitos.find();
        all += await Consumo.find();
        return all;
    },

    getDashData : async function () {
        let medias = {cad: {pecas: 0, tempo: 0},
                            def: {defeitos: 0},
                            cons: {agua: 0, tempo: 0}};
        let lastResults = {};
        // Cadencia
        let cadencias = await Cadencia.find();
        var date = new Date(0);
        let i = 0;
        cadencias.forEach(function(cad) {
            if (date < cad.data) {
                lastResults.cad = cad;
            }
            i++;
            medias.cad.pecas += cad.pecas;
            medias.cad.tempo += cad.tempo;
        });
        if (i != 0) {
            medias.cad.pecas /= i;
            medias.cad.tempo /= i;
        }
        // Defeitos
        let defeitos = await Defeitos.find();
        date = new Date(0);
        i = 0;
        defeitos.forEach(function(def) {
            if (date < def.data) {
                lastResults.def = def;
            }
            i++;
            medias.def.defeitos += def.defeitos;
        });
        if (i != 0) {
            medias.def.defeitos /= i;
        }
        // Consumo de água
        let consumo = await Consumo.find();
        date = new Date(0);
        i = 0;
        consumo.forEach(function(cons) {
            if (date < cons.data) {
                lastResults.cons = cons;
            }
            i++;
            medias.cons.agua += cons.agua;
            medias.cons.tempo += cons.tempo;
        });
        if (i != 0) {
            medias.cons.agua /= i;
            medias.cons.tempo /= i;
        }
        
        return {medias, lastResults};
    }
};
