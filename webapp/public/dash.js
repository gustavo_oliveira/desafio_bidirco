const updateData = async() => {
    const response = await fetch('/dashupdate', {
        method: 'get'
    });
    const data = await response.json();
    document.getElementById('txtmedpecas').innerHTML = data.medias.cad.pecas;
    document.getElementById('txtmedcadtemp').innerHTML = data.medias.cad.tempo;
    document.getElementById('txtmeddef').innerHTML = data.medias.def.defeitos;
    document.getElementById('txtmedlitros').innerHTML = data.medias.cons.agua;
    document.getElementById('txtmedcontemp').innerHTML = data.medias.cons.tempo;

    if (typeof data.lastResults.cad != "undefined") {
        document.getElementById('txtlastpecas').innerHTML = data.lastResults.cad.pecas;
        document.getElementById('txtlastcadtemp').innerHTML = data.lastResults.cad.tempo;
    }
    if (typeof data.lastResults.def != "undefined") {
        document.getElementById('txtlastdef').innerHTML = data.lastResults.def.defeitos;
    }
    if (typeof data.lastResults.cons != "undefined") {
        document.getElementById('txtlastlitros').innerHTML = data.lastResults.cons.agua;
        document.getElementById('txtlastcontemp').innerHTML = data.lastResults.cons.tempo;
    }
};

window.onload = updateData;
setInterval(updateData, 10000);