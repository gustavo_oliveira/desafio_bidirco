
var inputs = document.getElementsByTagName('input');
for (var i=0; i< inputs.length; i++){
    console.log(inputs[i]);
    inputs[i].onfocus = function(){
        if (this.value == "0")
            this.value = "";
    };
    inputs[i].onblur = function(){
        if (this.value == "")
            this.value = 0;
    }
}


const updateData = async() => {
    const response = await fetch('/update', {
        method: 'get'
    });
    const data = await response.json();
    document.getElementById('txtcadencia').innerHTML = data.cadencia;
    document.getElementById('txtdefeitos').innerHTML = data.defeitos;
    document.getElementById('txtconsumo').innerHTML = data.consumo;
};

window.onload = updateData;
setInterval(updateData, 1000);