const express = require('express');
const app = express();
const bodyParser = require('body-parser')

var mongo = require('./database/mongo.js');

app.set('view engine', 'pug');

app.use(express.static(__dirname + '/public'));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: true
}));

const server = app.listen(7000, () => {
  console.log(`Express running → PORT ${server.address().port}`);
});


app.get('/', (req, res) => {
  res.render('index', {
    title: 'Homepage'
  });
});

app.get('/dashboard', (req, res) => {
  res.render('dashboard', {
    title: 'Dashboard'
  });
});

app.post('/cadencia', function (req, res) { 
  var cadencia = req.body.cadencia;
  if (Object.keys(cadencia).length == 0)
    console.log("Empty request");
  else
    mongo.saveCadencia(cadencia.pecas, cadencia.tempo);
  res.redirect('/');
});

app.post('/defeitos', function (req, res) { 
  var result = req.body.defeitos;
  if (Object.keys(result).length == 0)
    console.log("Empty request");
  else
    mongo.saveDefeitos(result.defeitos);
  res.redirect('/');
});

app.post('/consumo', function (req, res) { 
  var consumo = req.body.consumo;
  if (Object.keys(consumo).length == 0)
    console.log("Empty request");
  else
    mongo.saveConsumo(consumo.litros, consumo.tempo);
  res.redirect('/');
});

app.get('/delete', function (req, res) { 
  mongo.deleteAll();
  res.redirect('/');
});

app.get('/update', async function (req, res) {
  const cadencia = await mongo.getCadencia();
  const defeitos = await mongo.getDefeitos();
  const consumo = await mongo.getConsumo();

  const response = {cadencia, defeitos, consumo};
  res.json(response);
});

app.get('/dashupdate', async function (req, res) {
  const response = await mongo.getDashData();
  res.json(response);
});

app.get('/viewall', async function (req, res) { 
  const response = await mongo.seeAll();
  res.json(response);
});