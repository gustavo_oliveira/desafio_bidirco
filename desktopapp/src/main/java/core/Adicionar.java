package core;

import java.io.DataOutputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.*;

public class Adicionar {

    public static void adicionar() {
        int i = 1;
        Scanner in = new Scanner(System.in);
        while (i != 0) {
            System.out.println("\n");
            System.out.println("  1. Cadência de fabrico");
            System.out.println("  2. Peças com defeitos");
            System.out.println("  3. Consumo de água");
            System.out.println();
            System.out.println("  0. Voltar");

            System.out.print("\nSelecione uma opção: ");
            i = in.nextInt();
            switch (i) {
                case 1: addDados("cadencia");
                    i = 0;
                    break;
                case 2: addDados("defeitos");
                    i = 0;
                    break;
                case 3: addDados("consumo");
                    i = 0;
                    break;
                case 0:
                    break;
                default:
                    System.out.println("Opção inválida");
            }

        }




    }

    private static void addDados(String tipo) {
        Map<String, List<Map<String, Integer>>> map = new HashMap<>();
        List<Map<String, Integer>> list = new ArrayList<>();
        Scanner in = new Scanner(System.in);

        switch (tipo) {
            case "cadencia":
                System.out.println("\nCadência de fabrico");
                // Pecas
                System.out.print("\nNúmero de peças: ");
                Map<String, Integer> pecas = new HashMap<String, Integer>();
                pecas.put("pecas", in.nextInt());
                list.add(pecas);
                // Tempo
                System.out.print("\nTempo de operação em segundos: ");
                Map<String, Integer> tempo = new HashMap<String, Integer>();
                tempo.put("tempo", in.nextInt());
                list.add(tempo);

                map.put("cadencia", list);

                break;
            case "defeitos":
                System.out.println("\nPeças com defeitos");
                // Defeitos
                System.out.print("\nNúmero de peças: ");
                Map<String, Integer> defeitos = new HashMap<String, Integer>();
                defeitos.put("defeitos", in.nextInt());
                list.add(defeitos);

                map.put("defeitos", list);

                break;
            case "consumo":
                System.out.println("\nConsumo de água");
                // litos
                System.out.print("\nLitros de água: ");
                Map<String, Integer> agua = new HashMap<>();
                agua.put("litros", in.nextInt());
                list.add(agua);
                // Tempo
                System.out.print("\nTempo de operação em segundos: ");
                tempo = new HashMap<>();
                tempo.put("tempo", in.nextInt());
                list.add(tempo);

                map.put("consumo", list);

                break;
        }

        postRequest(map);
    }

    private static void postRequest(Map<String, List<Map<String, Integer>>> map) {
        String request = "http://localhost:7000/";
        String urlParameters  = "";
        for (String tipo : map.keySet()) {
            request = request.concat(tipo);
            int i = 0;
            for (Map<String, Integer> m : map.get(tipo)) {
                for (String s : m.keySet()) {
                    if (i + 1 == map.get(tipo).size()) {
                        urlParameters = urlParameters.concat(tipo + "[" + s + "]=" + m.get(s));
                    } else {
                        urlParameters = urlParameters.concat(tipo + "[" + s + "]=" + m.get(s) + "&");
                    }
                    i++;
                }
            }
        }

        try {
            byte[] postData = urlParameters.getBytes(StandardCharsets.UTF_8);
            int postDataLength = postData.length;
            URL url = new URL(request);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoOutput(true);
            connection.setInstanceFollowRedirects(false);
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            connection.setRequestProperty("charset", "utf-8");
            connection.setRequestProperty("Content-Length", Integer.toString(postDataLength));
            connection.setUseCaches(false);
            try(DataOutputStream wr = new DataOutputStream(connection.getOutputStream())) {
                wr.write( postData );
            }
            String resposta = connection.getResponseMessage();
            //System.out.println("Resposta: " + resposta);

            connection.disconnect();
        } catch (ProtocolException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
