package core;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Map;
import java.util.Scanner;

public class Menu {

    private void show() {
        System.out.println("\nFabrica\n");
        System.out.println("  1. Consultar Dados");
        System.out.println("  2. Adicionar Dados");
        System.out.println("  3. Dashboard");
        System.out.println();
        System.out.println("  0. Sair");
    }

    public void selection() {
        int i = 1;
        Scanner in = new Scanner(System.in);
        while (i != 0) {
            show();
            System.out.print("\nSelecione uma opção: ");
            i = in.nextInt();
            switch (i) {
                case 1: Consultar.conultar();
                    break;
                case 2: Adicionar.adicionar();
                    break;
                case 3: Consultar.dashboard();
                    break;
                case 0:
                    System.out.println("Bye");
                    break;
                default:
                    System.out.println("Opção inválida");
            }
        }
    }



}
