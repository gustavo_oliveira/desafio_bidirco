package core;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class Consultar {

    public static void conultar() {
        JSONObject object = response("update");

        System.out.println("\n\nCadência de fabrico: " + object.getFloat("cadencia") + " (nr peças/hora)");
        System.out.println("Peças com defeitos: " + object.getFloat("defeitos"));
        System.out.println("Consumo de água: " + object.getFloat("consumo") + " (m3/hora)");
        System.out.println();
    }

    public static void dashboard() {
        JSONObject object = response("dashupdate");

        System.out.println("\n");
        System.out.println("Médias:");
        JSONObject medias = object.getJSONObject("medias");
        mostrarDashData(medias);
        System.out.println("LastResults:");
        JSONObject lastResults = object.getJSONObject("lastResults");
        mostrarDashData(lastResults);
    }

    private static void mostrarDashData(JSONObject object) {
        System.out.println("\nCadência de fabrico: ");
        System.out.println("   Nr de peças " + object.getJSONObject("cad").getFloat("pecas"));
        System.out.println("   Tempo " + object.getJSONObject("cad").getFloat("tempo"));
        System.out.println("Peças com defeitos: \n   " + object.getJSONObject("def").getFloat("defeitos"));
        System.out.println("Consumo de água: ");
        System.out.println("   Litros de água: " + object.getJSONObject("cons").getFloat("agua"));
        System.out.println("   Tempo: " + object.getJSONObject("cons").getFloat("tempo"));
        System.out.println();
    }

    private static JSONObject response(String request) {
        JSONObject object = new JSONObject();

        try {
            URL url = new URL("http://localhost:7000/" + request);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");
            connection.setConnectTimeout(5000);
            connection.setReadTimeout(5000);

            BufferedReader in = new BufferedReader(
                    new InputStreamReader(connection.getInputStream()));
            String inputLine;
            StringBuffer content = new StringBuffer();
            while ((inputLine = in.readLine()) != null) {
                content.append(inputLine);
            }
            in.close();

            object = new JSONObject(content.toString());

            connection.disconnect();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        //System.out.println("Objeto " + object.toString());
        return object;
    }
}
